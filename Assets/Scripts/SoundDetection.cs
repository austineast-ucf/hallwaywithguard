﻿using UnityEngine;
using System.Collections;

public class SoundDetection : MonoBehaviour {

	public float fieldOfHearing;
	public bool playerHeard; 

	private AudioSource audioSource;
	private SphereCollider soundCollider;
		
	// Use this for initialization
	void Start () {
		soundCollider = GetComponent<SphereCollider> ();
		audioSource = GetComponent<AudioSource> ();
		soundCollider.radius = fieldOfHearing;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider target) {
		if (target.gameObject.tag == "Player" && !playerHeard) {
			playerHeard = true;
			audioSource.Play ();
		}
	}
}
