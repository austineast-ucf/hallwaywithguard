﻿using UnityEngine;
using System.Collections;


public class CrouchController : MonoBehaviour
{
	public float CrouchHeight = 0.5f;
	public float CrouchSmoothTime = 0.5f;

	Vector3 m_StandingPosition;
	Vector3 m_CrouchingPosition;

	void Awake()
	{
		// Assume starting height is standing height.
		m_StandingPosition = transform.localPosition;

		// Move that position down to find crouching position.
		m_CrouchingPosition = transform.localPosition;
		m_CrouchingPosition.y = CrouchHeight;
	}

	void Update()
	{
		// Decide what height you want the camera to be moving towards this
		// frame.

		Vector3 targetPosition;

		// Assume starting height is standing height.
		m_StandingPosition = transform.localPosition;

		// Move that position down to find crouching position.
		m_CrouchingPosition = transform.localPosition;
		m_CrouchingPosition.y = CrouchHeight;

		if( Input.GetKey(KeyCode.LeftControl))
		{
			targetPosition = m_CrouchingPosition;
			transform.localPosition = targetPosition;
		}
		else if( Input.GetKeyUp(KeyCode.LeftControl))
		{
			targetPosition = m_StandingPosition;
			transform.localPosition = targetPosition;
		}

		// Smoothly move the camera towards that position.



	}
}