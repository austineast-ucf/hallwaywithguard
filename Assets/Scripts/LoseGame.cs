﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoseGame : MonoBehaviour {

	Text text;

	public Camera cam1; 
	public Camera cam2;

	Patrol patrol1;
	Patrol patrol2;

	PickUp pickup;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();
		patrol1 = GameObject.FindGameObjectWithTag ("Enemy1").GetComponent<Patrol> ();
		patrol2 = GameObject.FindGameObjectWithTag ("Enemy2").GetComponent<Patrol> ();
		pickup = GameObject.FindGameObjectWithTag ("Keycard").GetComponent<PickUp> ();

		cam1.enabled = true;
		cam2.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (pickup.playerDead) {
			cam2.enabled = true;
			text.text = "You Win! Restarting in " + pickup.restartTimer.ToString ("f0") + " seconds.";
		} else if (patrol1.playerDead) {
			cam2.enabled = true;
			text.text = "You've lost. Restarting in " + patrol1.restartTimer.ToString ("f0") + " seconds.";
		} else if (patrol2.playerDead) {
			cam2.enabled = true;
			text.text = "You've lost. Restarting in " + patrol2.restartTimer.ToString ("f0") + " seconds.";
		} 
	}
}
