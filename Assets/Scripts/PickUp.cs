﻿using UnityEngine;
using System.Collections;

public class PickUp : MonoBehaviour {
	
	GameObject mainCamera;
	GameObject carriedObject;

	bool carrying;
	public float distance;
	public float smooth;
	public bool playerDead;
	public float restartTimer;

	// Use this for initialization
	void Start () {
		mainCamera = GameObject.FindWithTag ("MainCamera");
	}

	void Update() {
		if (playerDead) {
			restartTimer -= Time.deltaTime;
			if (restartTimer <= 0)
				UnityEngine.SceneManagement.SceneManager.LoadScene ("TestScene");
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (carrying && mainCamera != null) {
			carry (carriedObject);
		} else {
			pickup ();
		}
	}

	void OnTriggerEnter(Collider target) {
		if (target.gameObject.tag == "Finish") {
			playerDead = true;
			restartTimer = 5f;
			Destroy (target.gameObject);
		}
	}

	void carry(GameObject o) {
		o.GetComponent<Rigidbody>().isKinematic = true;
		o.transform.position =  mainCamera.transform.position + mainCamera.transform.forward * distance;
		//o.transform.position = Vector3.Lerp(o.transform.position, mainCamera.transform.position + mainCamera.transform.forward * distance, Time.deltaTime * smooth);
	}

	void pickup() {
		if (Input.GetMouseButton(0)) {
			int x = Screen.width / 2;
			int y = Screen.height / 2;

			Ray ray = mainCamera.GetComponent<Camera> ().ScreenPointToRay (new Vector3 (x, y));
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit)) {
				PickUp p  = hit.collider.GetComponent<PickUp>();
				if (p != null) {
					carrying = true;
					carriedObject = p.gameObject;
				}
			}
		}
	}
}
