﻿using UnityEngine;
using System.Collections;


public class Patrol : MonoBehaviour {

	public Transform[] points;
	public float delay;
	public float delayTimer;
	private int destPoint = 0;

	private NavMeshAgent agent;
	private Animator anim;
	private SoundDetection soundDetection;
	private GameObject player;

	private bool idle;
	private bool running;

	public bool playerDead;
	public float restartTimer;

	void Start () {
		agent = GetComponent<NavMeshAgent>();
		anim = GetComponent<Animator> ();
		soundDetection = GetComponent<SoundDetection> ();
		player = GameObject.FindGameObjectWithTag ("Player");

		GotoNextPoint();
	}


	void GotoNextPoint() {
		// Returns if no points have been set up
		if (points.Length == 0)
			return;

		// Set the agent to go to the currently selected destination.
		agent.destination = points[destPoint].position;

		// Choose the next point in the array as the destination,
		// cycling to the start if necessary.
		destPoint = (destPoint + 1) % points.Length;
	}


	void Update () {

		anim.SetBool ("isIdle", idle);
		anim.SetBool ("isRunning", running);

		if ( playerDead) {
			restartTimer -= Time.deltaTime;
			if (restartTimer <= 0)
				UnityEngine.SceneManagement.SceneManager.LoadScene ("TestScene");
		}
		else if (soundDetection.playerHeard) {
			idle = false;
			running = true;
			agent.speed = 5f;
			agent.destination = player.transform.position;
		} else {
			// Choose the next destination point when the agent gets
			// close to the current one.
			if (agent.remainingDistance < 0.5f) {
				delayTimer -= Time.deltaTime;
				idle = true;
				if (delayTimer <= 0) {
					idle = false;
					GotoNextPoint ();
				}
			} else {
				delayTimer = delay;
			}
		}
	}
	void OnCollisionEnter(Collision target) {
		if (target.gameObject.tag == "Player") {
			playerDead = true;
			restartTimer = 5f;
			Destroy (target.gameObject);
		}
			
	}
}